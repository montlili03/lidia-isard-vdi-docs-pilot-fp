# Introducción

Bienvenido a la documentación de [IsardVDI](https://isardvdi.com) para la instalación del Departament d'Educació de la Generalitat de Catalunya. Este manual se focaliza en las funcionalidades específicas de esta instalación. El resto de funcionalidades se encuentra en: [Manual](https://isard.gitlab.io/isardvdi-docs/)