# Introducció

Benvingut a la documentació de [IsardVDI](https://isardvdi.com) per a la instal·lació del Departament d'Educació de la Generalitat de Catalunya. Aquest manual es focalitza en les funcionalitats específiques d'aquesta instal·lació. La resta de funcionalitats es troba en: [Manual](https://isard.gitlab.io/isardvdi-docs/)
