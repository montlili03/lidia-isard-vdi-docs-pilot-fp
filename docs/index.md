# Introduction

Welcome to the [IsardVDI](https://isardvdi.com) documentation for the installation of the Department of Education of the Generalitat de Catalunya. This manual focuses on the specific functionalities of this installation. The rest of the functionalities can be found in: [Manual](https://isard.gitlab.io/isardvdi-docs/)