# SAML Access

To be able to access, for example, from an education department account through a registration code provided by a Manager.

From the main page, select the category to which the user will belong and press the button ![](./saml_access.ca.images/saml_access1.png)

![](./saml_access.images/saml_access1.png)

It is redirected to the login portal and filled with the mail.

![](./saml_access.images/saml_access2.png)

Enter the password for the account and click the "Sign in" button.

![](./saml_access.images/saml_access3.png)

If you want to keep the session started, press the "Yes" button.

![](./saml_access.images/saml_access4.png)


## Registration by registration code

If this is the first time you are accessing as a user, the form will appear to enter a registration code.

The registration code is created by the organization manager and how it is generated is explained later in the "Manager" section of the documentation. Each user must belong to a group and have a role, be it advanced (teacher), user (student), etc. For each group of users, self-registration codes can be generated for different roles.

The code provided by a Manager is entered and the button is pressed ![](./saml_access.images/saml_access6.png)

![](./saml_access.images/saml_access5.png)

And you access the basic interface authenticated with the SAML account.

![](./oauth_access.images/home1.png)

Once authenticated, there is no need to repeat the registration process. When you want to access again through SAML, you just have to click on the button ![](./saml_access.ca.images/saml_access1.png) and use the account credentials.